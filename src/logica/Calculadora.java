package logica;

public class Calculadora {
	private int valor1;
	private int valor2;
	
	public Calculadora(int valor1,int valor2) {
		this.valor1=valor1;
		this.valor2=valor2;
	}
		
	public int getValor1() {
		return valor1;
	}
	public void setValor1(int valor1) {
		this.valor1 = valor1;
	}

	public int getValor2() {
		return valor2;
	}

	public void setValor2(int valor2) {
		this.valor2 = valor2;
	}

	
	public int calcular(int op){
		int respuesta = 1;
		if(op==1) {
			respuesta=this.valor1+this.valor2;
		}else if(op==2) {
			respuesta=this.valor1-this.valor2;
		}else if(op==3) {
			respuesta=this.valor1*this.valor2;
		}else if(op==4) {
			if(valor2!=0) {
				respuesta=this.valor1/this.valor2;
			}else {
				respuesta=0;
			}
		}else if(op==5) {
			for(int i=1;i<=valor1;i++) {
				respuesta*=i;
			}
		}else if(op==6) {;
			for(int i=0;i<valor2;i++) {
				respuesta*=valor1;
			}
		}
		
		return respuesta;
	}
	
	
}
